package com.zing.transactionservice.exception;

import lombok.Data;

@Data
public class CustomTransactionException extends RuntimeException{


    private String errorCode;

    public CustomTransactionException(String message, String errorCode)
    {
        super(message);
        this.errorCode = errorCode;
    }

}
