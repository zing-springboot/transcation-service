package com.zing.transactionservice.entity;

import com.zing.transactionservice.dto.Category;
import com.zing.transactionservice.dto.PaymentType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer transactionId;

    @Enumerated(EnumType.STRING)
    private PaymentType paymentType;

    private String description;

    @Enumerated(EnumType.STRING)
    private Category category;

    private LocalDate date;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "customerId")
    private Customer customer;

    private double amount;

}
