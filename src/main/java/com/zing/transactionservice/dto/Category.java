package com.zing.transactionservice.dto;

public enum Category {
    GROCERIES,HEALTH,ENTERTAINMENT,TRAVEL,FOOD,CLOTHS;
}
