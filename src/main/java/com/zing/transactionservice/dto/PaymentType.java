package com.zing.transactionservice.dto;

public enum PaymentType {
    DEBIT,CREDIT;
}
